package ru.t1.azarin.tm.command.user;

import ru.t1.azarin.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    public final static String NAME = "user-view-profile";

    public final static String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW USER PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
