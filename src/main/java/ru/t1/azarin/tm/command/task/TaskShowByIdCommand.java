package ru.t1.azarin.tm.command.task;

import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    public final static String NAME = "task-show-by-id";

    public final static String DESCRIPTION = "Show task by id.";

    @Override
    public void execute() {
        System.out.println("[FIND TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneById(id);
        showTask(task);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
