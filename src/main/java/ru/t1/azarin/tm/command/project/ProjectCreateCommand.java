package ru.t1.azarin.tm.command.project;

import ru.t1.azarin.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public final static String NAME = "project-create";

    public final static String DESCRIPTION = "Create new project.";

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().create(name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
