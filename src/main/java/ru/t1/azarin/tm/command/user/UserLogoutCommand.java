package ru.t1.azarin.tm.command.user;

public class UserLogoutCommand extends AbstractUserCommand {

    public final static String NAME = "user-logout";

    public final static String DESCRIPTION = "Logout current user.";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
