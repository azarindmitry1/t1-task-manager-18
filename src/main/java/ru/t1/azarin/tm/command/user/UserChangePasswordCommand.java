package ru.t1.azarin.tm.command.user;

import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    public final static String NAME = "user-change-password";

    public final static String DESCRIPTION = "Change user password.";

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(user.getId(), newPassword);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
