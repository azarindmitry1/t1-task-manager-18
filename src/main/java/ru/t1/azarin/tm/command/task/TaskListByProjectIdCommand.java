package ru.t1.azarin.tm.command.task;

import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    public final static String NAME = "task-show-by-project-id";

    public final static String DESCRIPTION = "Show task by project id.";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getTaskService().findAllByProjectId(id);
        renderTask(tasks);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
