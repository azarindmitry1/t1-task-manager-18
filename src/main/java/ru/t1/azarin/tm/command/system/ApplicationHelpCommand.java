package ru.t1.azarin.tm.command.system;

import ru.t1.azarin.tm.api.model.ICommand;
import ru.t1.azarin.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    public final static String NAME = "help";

    public final static String ARGUMENT = "-h";

    public final static String DESCRIPTION = "Display application command.";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
