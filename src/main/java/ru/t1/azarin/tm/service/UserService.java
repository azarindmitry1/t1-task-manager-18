package ru.t1.azarin.tm.service;

import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.IUserService;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.exception.entity.UserNotFoundException;
import ru.t1.azarin.tm.exception.field.EmailEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.LoginEmptyException;
import ru.t1.azarin.tm.exception.field.PasswordEmptyException;
import ru.t1.azarin.tm.exception.user.ExistEmailException;
import ru.t1.azarin.tm.exception.user.ExistLoginException;
import ru.t1.azarin.tm.exception.user.RoleEmptyException;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User add(final User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.add(user);
    }

    @Override
    public User remove(final User user) {
        if (user == null) throw new UserNotFoundException();
        userRepository.remove(user);
        return user;
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = userRepository.findById(id);
        return remove(user);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = userRepository.findByLogin(login);
        return remove(user);
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = userRepository.findByEmail(email);
        return remove(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return userRepository.create(login, password);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistEmailException();
        return userRepository.create(login, password, email);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        return userRepository.create(login, password, role);
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isLoginExist(login);
    }

    @Override
    public boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) return false;
        return userRepository.isEmailExist(email);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userRepository.findById(id);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(
            final String id,
            final String firstName,
            final String middleName,
            final String lastName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = userRepository.findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

}
