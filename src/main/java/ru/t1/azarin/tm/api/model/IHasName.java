package ru.t1.azarin.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}