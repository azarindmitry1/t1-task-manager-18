package ru.t1.azarin.tm.api.repository;

import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    User remove(User user);

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}
